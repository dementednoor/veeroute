Имеющиеся классы:

    1) `Process`. При создании объекта класса автоматически создается массив  
    событий данного процесса (`self.events`). Класс имеет методы `current_event`, 
    который возвращает  текущее активное событие, `start`, `stop` и `reanimate` 
    для запуска, остановки и возобновления процесса соответственно, 
    а также `add_event`, который добавляет событие в массив событий процесса;

    2) `Performer`. Исполнитель инициализируется с именем и ролью. Может создавать
    события (`create_event`), менять состояние события (`change_event_state`), добавлять
    новые свойства события (`add_new_property`), а также исполнять событие 
    (`perform_event`);

    3) Event. Все события инициализируются исполнителями. При инициализации
    задается имя, исполнитель и его роль.

Глобальные функции:

    1) `print_process_events` - выводит все события и состояния конкретного 
    процесса;

    2) `get_process_info` - выводит информацию по всем имеющимся в системе 
    процессам и событиям этих процессов.

В разделе main представлена демонстрация работы перечисленных выше классов и 
функций. В лог заносится информация о создании, изменении свойств новых процессов,
пользователей, событий и т. д.

    Примечания: 
    - выполнение только одного события в определенный момент времени реализовано 
    в функции `change_event_state` класса `Performer`;
    - в той же функции предусмотрено, что состояние события может менять только 
    тот пользователь, который это событие создал;
    - есть строгие состояния, описанные в списке `Event_state_list` в самом начале;
    - новые свойства события задаются только исполнителем с ролью "admin".;
    - впоследствии при большом количестве процессов, событий и исполнителей 
    можно выносить эту информацию в таблицы баз данных (например:
    `CREATE TABLE Performer(event_number integer, event_name text, role text)` и т. д.);
    - в данный момент последовательность выполнения событий задается
    циклически по массиву `events`, в дальнейшем класс `Event` может содержать 
    поле `next_event` с указателем на событие, которое будет выполнено следующим.
from datetime import datetime
import logging

Processes = []
Event_state_list = ['active', 'inactive', 'frozen', 'active, frozen', 'shut down']
logging.basicConfig(filename='log.log', filemode='w', level=logging.INFO)
fmt = '%d.%m.%y %H:%M:%S'


def log_fill(message):
    logging.info('{} {}'.format(datetime.now().strftime(fmt), message))


class Process:
    def __init__(self, name):
        self.name = name
        self.events = []  # initializing array of events
        Processes.append(self)
        log_fill("A '{}' process was created.".format(self.name))

    def current_event(self):
        for ev in self.events:
            if ev.state == 'active':
                return ev

    def add_event(self, ev):
        self.events.append(ev)
        ev.__setattr__('process', self)
        log_fill("A new event '{}' was added to the '{}' process.".format(ev.name, self.name))

    def start(self):
        for e in self.events:
            if e.state == 'active':
                e.performer.perform_event(e)

    def stop(self):
        for e in self.events:
            if e.state == 'active':
                e.state = e.state + ', frozen'  # to know what event was active to start from it when the whole
                # process reanimates
            else:
                e.state = 'frozen'
        log_fill("A '{}' process was stopped".format(self.name))

    def reanimate(self):
        for e in self.events:
            if e.state == 'frozen':  # after process stoppage all events are either frozen or active, frozen
                e.state = 'inactive'
            else:
                e.state = 'active'
        log_fill("A '{}' process was reanimated".format(self.name))


class Performer:
    def __init__(self, name, role):
        self.name = name
        self.role = role

    def create_event(self, event_name, role):
        print("{} is performing '{}' with '{}' role".format(self.name, event_name, role))
        log_fill(("{} is performing '{}' with '{}' role".format(self.name, event_name, role)))
        _event = Event(event_name, self, self.role)
        return _event

    def change_event_state(self, event_name, new_state):
        if new_state in Event_state_list:
            #  if there is one active event inside the same process, we cannot start another one
            parallel_activity = False
            if new_state == 'active':
                for e in event_name.process.events:
                    if e.name != event_name.name and e.state == 'active':
                        print("Error. '{}' is still active. Two events cannot run simultaneously.".format(e.name))
                        parallel_activity = True
            if not parallel_activity:
                if self.name == event_name.performer.name:  # checking if user has rights to do anything with the event
                    #  print("{} has changed {}'s state from {} to {}".
                    #  format(self.name, event_name.name, event_name.state, new_state))
                    event_name.state = new_state
                    log_fill("Performer {} has changed event '{}' state to '{}'".
                             format(self.name, event_name.name, new_state))
                    if new_state == 'active':
                        self.perform_event(event_name)

                else:
                    print('This user is not allowed to change this event.')
        else:
            print("This state doesn't exist. Please, try again.")

    def add_new_property(self, event_name, prop_name, prop_state):
        if self.role == 'admin':
            if self.name == event_name.performer.name:
                event_name.__setattr__(prop_name, prop_state)
                log_fill("Performer {} added new property '{}' to the '{}' event".
                         format(self.name, prop_name, event_name.name))
            else:
                print('This user is not allowed to change this event.')
        else:
            print("User's role doesn't allow to add new properties to event.")

    def perform_event(self, _event):
        res = None  # it's an abstract result yet
        # do smth with event, calculating some result
        log_fill("{} is performing '{}' event".format(self.name, _event.name))
        # _event.state = 'done'  - could be a definition of performed event, for example
        return res


class Event:
    def __init__(self, name, performer, role):
        self.name = name
        self.performer = performer
        self.state = 'inactive'
        log_fill("An event '{}' was created by {} with '{}' role".format(name, performer.name, role))


def print_process_events(process):
    for p in process.events:
        print('event - {}, state - {}'.format(p.name, p.state))


def get_process_info():
    for p in Processes:
        print('process: {}'.format(p.name))
        print('Events:')
        for e in p.events:
            print('name: {}, state: {}'.format(e.name, e.state))
        print('\n')


if __name__ == '__main__':
    P = Process('Task')
    print('\n{}Creating new performers and events:{}'.format('-'*20, '-'*20))
    # create two users with different roles
    vas = Performer('Vasyly', 'admin')
    ser = Performer('Sergey', 'moderator')
    # these two users create one event respectively
    event = vas.create_event('create a task', vas.role)
    event2 = ser.create_event('solve the task', ser.role)
    print('\n{}Adding events to the process:{}'.format('-' * 20, '-' * 20))
    print('Before: \n{}'.format(print_process_events(P)))
    P.add_event(event)  # add two events to one process
    P.add_event(event2)
    print('\nAfter:')
    print_process_events(P)
    print("\n{}Changing events' state:{}".format('-' * 20, '-' * 20))
    print('Before:')
    print(event.state)
    vas.change_event_state(event, 'active')  # activating one event
    print('\nAfter:')
    print(event.state)
    print('\n{}Trying to change events state by user without rights:{}'.format('-' * 20, '-' * 20))  # user ser is not
    # allowed to change event's state
    ser.change_event_state(event, 'shut down')
    print('\n{}Adding new property:{}'.format('-' * 20, '-' * 20))
    print('Before:')
    try:
        print(event.mutability)
    except AttributeError as e:  # cause object doesn't have this property yet
        print(e)
    vas.add_new_property(event, 'mutability', 'mutable')
    print('\nAfter:')
    print(event.mutability)
    print('\n{}Trying to run two task in parallel:{}'.format('-' * 20, '-' * 20))
    print('Before:')
    print_process_events(P)
    print('\nAfter:')
    vas.change_event_state(event, 'active')
    ser.change_event_state(event2, 'active')  # event is still active so event2 cannot be started
    print_process_events(P)
    P.start()  # virtually starting to implement some operations within the events
    print('\n{}Stopping the process:{}'.format('-' * 20, '-' * 20))
    print('Before:')
    print_process_events(P)
    P.stop()  # stopping the whole process
    print('\nAfter:')
    print_process_events(P)
    print('\n{}Reanimating the process:{}'.format('-' * 20, '-' * 20))
    P.reanimate()
    print_process_events(P)
    print('\n{}Printing processes info:{}'.format('-' * 20, '-' * 20))
    get_process_info()
    print('\n{}Getting active event within one process:{}'.format('-' * 20, '-' * 20))
    print("Process: {}, event: {}".format(P.name, P.current_event().name))
    print('Hello World')